variable "backend_name" {
  description = "name of the backend"
  nullable    = false
  type        = string

  validation {
    condition     = length(var.backend_name) >= 3 && length(var.backend_name) <= 63
    error_message = "Bucket names must be between 3 (min) and 63 (max) characters long"
  }

  validation {
    condition     = can(regex("^[a-z0-9.-]{1,64}$", var.backend_name))
    error_message = "Bucket names can consist only of lowercase letters, numbers, dots (.), and hyphens (-)."
  }

  validation {
    condition     = can(regex("^[a-z0-9].*[a-z0-9]$", var.backend_name))
    error_message = "Bucket names must begin and end with a letter or number"
  }

  validation {
    condition     = !strcontains(var.backend_name, "..")
    error_message = "Bucket names must not contain two adjacent periods."
  }

  validation {
    condition     = !can(regex("^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$", var.backend_name))
    error_message = "Bucket names must not be formatted as an IP address (for example, 192.168.5.4)"
  }

  validation {
    condition     = !startswith(var.backend_name, "xn--")
    error_message = "Bucket names must not start with the prefix xn--"
  }

  validation {
    condition     = !startswith(var.backend_name, "sthree-")
    error_message = "Bucket names must not start with the prefix sthree-"
  }

  validation {
    condition     = !endswith(var.backend_name, "-s3alias")
    error_message = "Bucket names must not end with the suffix -s3alias."
  }

  validation {
    condition     = !endswith(var.backend_name, "--ol-s3")
    error_message = "Bucket names must not end with the suffix --ol-s3"
  }
}

variable "table_name" {
  description = "Name of the table"
  nullable    = false
  type        = string

  validation {
    condition     = length(var.table_name) >= 3 && length(var.table_name) <= 63
    error_message = "Table name must be between 3 (min) and 63 (max) characters long"
  }

  validation {
    condition     = can(regex("^[a-z0-9-]{1,64}$", var.table_name))
    error_message = "Table name can consist only of lowercase letters, numbers and hyphens (-)."
  }

  validation {
    condition     = can(regex("^[a-z].*[a-z]$", var.table_name))
    error_message = "Table name must begin and end with a letter"
  }

  validation {
    condition     = !strcontains(var.table_name, "--")
    error_message = "Table name must not contain two adjacent hyphens (-)."
  }

}

variable "backend_tags" {
  description = "optional tags for backend resources"
  nullable    = true
  type        = map(string)
}

variable "provider_region" {
  description = "region of provider"
  nullable    = false
  type        = string
}

variable "provider_profile" {
  description = "profile of the provider"
  nullable    = false
  type        = string
}

variable "project_name" {
  description = "name of the project"
  nullable    = false
  type        = string
}
variable "project_environment" {
  description = "environment of the project"
  nullable    = false
  type        = string
}
variable "project_created_by" {
  description = "name of the project resources creator"
  nullable    = false
  type        = string
}
variable "project_group" {
  description = "name of the project resources group"
  nullable    = false
  type        = string
}
