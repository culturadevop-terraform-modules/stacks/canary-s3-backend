output "s01_backend_bucket_id" {
  description = "Id of the created bucket s3"
  value       = module.backend.backend_bucket_id
}

output "s01_backend_bucket_arn" {
  description = "arn of the created bucket s3"
  value       = module.backend.backend_bucket_arn
}

output "s01_backend_bucket_name" {
  description = "name of the created bucket s3"
  value       = module.backend.backend_bucket_name
}

output "s01_backend_table_name" {
  description = "name of the storage table"
  value       = module.backend.backend_table_name
}

output "s01_backend_configuration_file_dot_env" {
  description = "generate the configuration file for the backend infrastructure"
  value       = module.backend.backend_configuration_file_dot_env
}

output "s01_project_backend_configuration_file_dot_env" {
  description = "generate the project configuration file for use remote backend state"
  value       = module.backend.project_backend_configuration_file_dot_env
}

output "s01_project_configuration_file_dot_env" {
  description = "generate the project configuration file .env"
  value       = module.backend.project_configuration_file_dot_env
}

output "s01_debug" {
  description = "For debug purpose."
  value       = module.backend.debug
}
