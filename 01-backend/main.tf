terraform {
  required_version = "~>1.7.1"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>5.35.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~>3.1.0"
    }
  }
}

locals {
  name = var.backend_name
  tags = try(var.backend_tags != null ? var.backend_tags : null, null)

  common_tags = {
    Project     = var.project_name
    Environment = var.project_environment
    CreatedBy   = var.project_created_by
    Group       = var.project_group
  }
}

provider "aws" {
  profile = var.provider_profile
  region  = var.provider_region
}


module "backend" {
  source  = "gitlab.com/culturadevop-terraform-modules/aws-s3-backend/aws"
  version = "0.1.0"

  project = {
    project     = var.project_name
    environment = var.project_environment
    createdBy   = var.project_created_by
    group       = var.project_group
  }

  resources = {
    bucket_name = var.backend_name
    table_name  = var.table_name
    tags = {
      Name        = "TestingBucket S3"
      Description = "TestingBucket S3"
    }
  }
}
