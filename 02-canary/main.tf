terraform {
  required_version = "~>1.7.1"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>5.35.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~>3.1.0"
    }
  }

  backend "s3" {
  }
}

provider "aws" {
  profile = var.provider_profile
  region  = var.provider_region
}

module "canary" {
  source  = "gitlab.com/culturadevop-terraform-modules/aws-canary/aws"
  version = "0.1.0"


  project = {
    project     = var.project_name
    environment = var.project_environment
    createdBy   = var.project_created_by
    group       = var.project_group
  }

  resources = {
    name                          = var.project_name
    blue_ami                      = "ami-xxxxxxxxxxxxxxxxxxxxxxxx"
    green_ami                     = "ami-xxxxxxxxxxxxxxxxxxxxxxxx"
    blue_user_data_file           = "${path.cwd}/scripts/blue_user_data.sh"
    green_user_data_file          = "${path.cwd}/scripts/green_user_data.sh"
    blue_instance_type            = "t3.micro"
    green_instance_type           = "t3.micro"
    traffic_distribution          = "split"
    vpc_id                        = "vpc-xxxxxxxx"
    instances_security_groups_ids = toset(["sg-xxxxxxxxxxxx"])
    alb_security_groups_ids       = toset(["sg-xxxxxxxxxxxx"])
    alb_subnets_ids               = toset(["subnet-xxxxxxx", "subnet-xxxxxxx"])

    tags = {
      Stack = "CanaryS3BackendStack"
    }
  }
}
