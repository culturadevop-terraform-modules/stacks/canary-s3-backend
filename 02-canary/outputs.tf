output "s02_canary_alb_endpoint" {
  description = "Public DNS of ALB"
  value       = module.canary.alb_endpoint
}

output "s02_canary_instances_ids" {
  description = "Ids of the created EC2 instances."
  value       = module.canary.instances_ids
}

output "s02_canary_instances_arns" {
  description = "Arns of the created EC2 instances."
  value       = module.canary.instances_arns
}

output "s02_canary_instances_ips" {
  description = "List of public IP addresses assigned to the instances, if applicable."
  value       = module.canary.instances_ips
}
