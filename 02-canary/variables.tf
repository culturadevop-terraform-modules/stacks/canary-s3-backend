variable "provider_region" {
  description = "region of provider"
  nullable    = false
  type        = string
}

variable "provider_profile" {
  description = "profile of the provider"
  nullable    = false
  type        = string
}

variable "project_name" {
  description = "name of the project"
  nullable    = false
  type        = string
}
variable "project_environment" {
  description = "environment of the project"
  nullable    = false
  type        = string
}
variable "project_created_by" {
  description = "name of the project resources creator"
  nullable    = false
  type        = string
}
variable "project_group" {
  description = "name of the project resources group"
  nullable    = false
  type        = string
}
